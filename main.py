from folder import folder
import sys
import json
import os

keys = {}

def encrypt(path):
        global keys
        x = folder(path)
        keys = x.encrypt()
        
        outfile = open("keys.json", "w")
        outfile.write(json.dumps(keys))
        outfile.close()         
                

def decrypt(path):
        global keys

        f = open("keys.json", "r")
        keys = json.load(f)
        f.close()
        os.remove("keys.json")

        x = folder(path)
        x.decrypt(keys)

def main():
    try:
        args = sys.argv[1:]
        #args = ["decrypt", "testFolder"]
        if(len(args) != 2):
                print("error! usage: python main.py <encrypt/decrypt> <path>")
                #return None
        elif(args[0] == "encrypt"):
                encrypt(args[1])
        elif(args[0] == "decrypt"):
                decrypt(args[1])
    except Exception as e:
            print(f"error! {e}")



if __name__ == '__main__':
    main()