import myFile
import os

class folder(myFile.myFile):
    def __init__(self, path):
        if(os.path.isdir(path)):
            self.path = path
            self.name = os.path.basename(path)
            self.filesAndFolders = []
            self.keys = {self.name: []}

            files = (file for file in os.listdir(path) if os.path.isfile(os.path.join(path, file)))
            dirs = (dir for dir in os.listdir(path) if os.path.isdir(os.path.join(path, dir)))

            for file in files:
                self.filesAndFolders.append(myFile.myFile(os.path.join(path, file)))
            
            for dir in dirs:
                self.filesAndFolders.append(folder(os.path.join(path, dir)))
        else:
            raise Exception("Path is not a directory")
    
    def encrypt(self):
        print("encrypting folder: " + self.name)
        for i in self.filesAndFolders:
            self.keys[self.name].append(i.encrypt())
        return self.keys

    def findFileOrDirByName(self, name):
        for i in self.filesAndFolders:
            if i.name == name:
                return i
        print("kaki")
        return None
    
    def decrypt(self, keys):
        print("decrypting folder: " + self.name)
        print(keys)
        
        for i in keys[self.name]:
            x = self.findFileOrDirByName(list(i.keys())[0])
            x.decrypt(i)
        