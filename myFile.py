import os
import random

class myFile:
    def __init__(self, path):
        if(os.path.isfile(path)):
            self.path = path
            self.name = os.path.basename(path)
            self.ext = os.path.splitext(path)[1]
            self.size = os.path.getsize(path)
            
        else:
            raise Exception("File not found")

    
    def generateKey(self):
        key = 0

        match self.size:
            case 1:
                key = 1
            case 2:
                key = 2
            case _:
                key = random.randint(2, 255) % self.size
                    
        return key
    

    def getEncryptingValues(self):
        #the function just organizes all of the things that are needed for the encryption
        j = {"key":self.generateKey(),"sizeLeft":self.size,"flag":False,"xorValue":0,"chunkSize":0}

        while(j["key"] == 0):
            j["key"] = self.generateKey()
        
        j["xorValue"] = (int(str(j["key"]) * 2) % 255)

        if(j["xorValue"] == 0 or j["xorValue"] == 1):
            j["xorValue"] = j["key"]
        
        j["chunkSize"] = self.size / j["key"]

        return j


    def encrypt(self,key=-1):
        print("Encrypting file: " + self.name)
        with open(self.path, 'rb') as f:
            data = f.read()
        data = bytearray(data)

        
        ev = self.getEncryptingValues()

        #print(ev)

        if(key != -1):
            ev["key"] = key

        ev["chunkSize"] = len(data) / ev["key"]
        
        flag = False

        for i in range(len(data)):
            if(i%ev["chunkSize"] == 0):
                flag = not flag
            if(flag):
                data[i] ^= ev["key"]
        with open(self.path, 'wb') as f:
            f.write(data)
        
        self.key = ev["key"]
        
        return {self.name: ev["key"]}
    
    def decrypt(self,keys):
        print(f"decrypting {self.name}")
        self.encrypt(keys[self.name])
        
    
    